#!/bin/sh -e
# https://faq.i3wm.org/questions/83/how-to-run-i3lock-after-computer-inactivity.1.html

# Take a screenshot
scrot /tmp/screen_locked.png

# Pixellate it 10x
convert /tmp/screen_locked.png -blur 0x6 /tmp/screen_locked.png

# Lock screen displaying this image.
i3lock -i /tmp/screen_locked.png

# Turn the screen off after a delay.
#sleep 60; pgrep i3lock && xset dpms force off
