# halberom's dotfiles

Intended for MacOS and Arch Linux.  Makefiles are used to run scripts and install packages, and [GNU Stow](https://www.gnu.org/software/stow/) is used to symlink config files and folders to the home directory.

## Usage

```
git clone https://github.com/halberom/dotfiles.git
make -f Makefile.$(uname)
```

## MacOS Config

2. open iTerm2 Preferences -> General,
3. select 'load preferences from a custom folder or URL' and enter the path to dotfiles/

