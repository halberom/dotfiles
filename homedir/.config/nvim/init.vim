" [c, ]c, l+hs, l+hr, l+hp   ;gitgutter
" F8                         ;tagbar
" F7                         ;gundotoogle
" gv                         ;vim reselect last block
" gc                         ;tcomment_vim
" ga                         ;vim-easy-align
" ctrl-n                     ;vim-multiple-cursors
" :magit                     ;vimagit, stage and commit
" :Gpush                     ;vim fugitive, git push

if &compatible
  set nocompatible
endif

call plug#begin('~/.config/nvim/plugged')
" syntax
Plug 'elzr/vim-json'
Plug 'hashivim/vim-terraform'
Plug 'fatih/vim-hclfmt'
Plug 'cespare/vim-toml'

" theme
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'Yggdroot/indentline'
Plug 'joshdick/onedark.vim'

" utils
Plug 'ervandew/supertab'
Plug 'mileszs/ack.vim'
Plug 'simnalamburt/vim-mundo'
Plug 'terryma/vim-multiple-cursors'
Plug 'tpope/vim-repeat'
Plug 'moll/vim-bbye'
Plug 'ap/vim-buftabline'

" file navigation
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" git
Plug 'tpope/vim-fugitive'               " git foo
Plug 'airblade/vim-gitgutter'

" text alteration
Plug 'junegunn/vim-easy-align'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'tomtom/tcomment_vim'

" coding
if has('nvim')
  function! DoRemote(arg)
    UpdateRemotePlugins
  endfunction

  Plug 'Shougo/deoplete.nvim', { 'do': function('DoRemote') }
  Plug 'zchee/deoplete-go', { 'do': 'make'}
  Plug 'zchee/deoplete-jedi'
  Plug 'neomake/neomake'
  Plug 'fishbullet/deoplete-ruby'
else
  Plug 'fatih/vim-go'
endif

call plug#end()

" handled by vim-plug
filetype plugin indent on

" make backspace work like in a normal program
set backspace=indent,eol,start
" ignore certain file types
set wildignore+=*/tmp/*,*.so,*.swp,*.zip
" highlight the line the cursor is on
"set cursorline
set lazyredraw
" allow yanking and pasting to the system clipboard
set clipboard=unnamed

set modeline
set modelines=5

" don't create swap file
set noswapfile
set noshowmode
" set relativenumber number
set conceallevel=0

set ignorecase
set incsearch
set hlsearch

" always display the status line
set laststatus=2
" to prevent a pause occurring before the status bar is updated after
" leaving insert mode
set ttimeoutlen=50

"setlocal foldmethod=syntax

" open split panes below and right, more natural
set splitbelow
set splitright

" set default tab
set expandtab
set shiftwidth=2
set tabstop=2

set hidden
nnoremap gn :bnext<CR>
nnoremap gp :bprev<CR>

"let g:python2_host_prog = '/usr/local/bin/python'
"let g:python3_host_prog = '/usr/local/bin/python3'

" enable spell checking
"set spell

let mapleader = "\<Space>"
let g:mapleader = "\<Space>"
"let mapleader = ","
"let g:mapleader = ","
nnoremap <Leader>w :w<CR>
map <C-a> :FZF<CR>
map <C-p> :GFiles<CR>
map <C-t> :Buffers<CR>
nmap <F2> :NERDTreeToggle<CR>
map <F3> :NERDTreeFind<CR>
map <Tab> <C-W>w
map <S-Tab> <C-W>W
nmap <F8> :TagbarToggle<CR>

nnoremap <silent> <Esc><Esc> :let @/=""<CR>

" for terminal emulation
" tnoremap <A-h> <C-\><C-n><C-w>h
" tnoremap <A-j> <C-\><C-n><C-w>j
" tnoremap <A-k> <C-\><C-n><C-w>k
" tnoremap <A-l> <C-\><C-n><C-w>l
" nnoremap <A-h> <C-w>h
" nnoremap <A-j> <C-w>j
" nnoremap <A-k> <C-w>k
" nnoremap <A-l> <C-w>l

if has('nvim')
  autocmd! BufWritePost * Neomake
endif

autocmd CompleteDone * pclose

set background=dark
colorscheme onedark
hi CursorLineNR guifg=#ffffff
hi SpellBad guibg=#ff2929 guifg=#ffffff ctermbg=224

hi clear SignColumn

"autocmd FileType vim setlocal foldmethod=marker
autocmd BufRead,BufNewFile */playbooks/*.yml set filetype=ansible
autocmd BufRead,BufNewFile */ansible/*.yml set filetype=ansible
" remove trailing whitespace on any line
"autocmd BufRead,BufWritePre,FileWritePre * silent! %s/[\r \t]\+$//
autocmd BufWritePost ~/.xbindkeysrc :! xbindkeys -f %

"let g:airline_theme='solarized'
let g:airline_theme='onedark'
" get rid of fancy chars
let g:airline_left_sep=''
let g:airline_right_sep=''
" change spacing with line:column section
let g:airline_section_z = '%3pp %l:%c'

" Use deoplete.
let g:deoplete#enable_at_startup = 1

let g:fzf_layout = { 'down': '~30%' }
let g:fzf_buffers_jump = 1
let $FZF_DEFAULT_COMMAND = 'ag --hidden --ignore .git -g ""'

set signcolumn=yes
let g:gitgutter_highlight_lines = 0
let g:gitgutter_max_signs = 500
nmap [h <Plug>GitGutterPrevHunk

let g:SuperTabDefaultCompletionType = "context"
let g:SuperTabContextDefaultCompletionType = "<c-n>"
"let g:SuperTabClosePreviewOnPopupClose = 1

" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"
"
" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit = "vertical"

let g:vim_json_syntax_conceal = 0

nmap ga <Plug>(EasyAlign)
xmap ga <Plug>(EasyAlign)

let g:ansible_unindent_after_newline = 1

" Open the error window automatically
let g:neomake_open_list = 2
let g:neomake_python_venvpylint_maker = {
  \ 'exe': 'python',
  \ 'args': [
    \ '-m',
    \ 'pylint',
      \ '-f', 'text',
      \ '--msg-template="{path}:{line}:{column}:{C}: [{msg_id}] [{symbol}] {msg}"',
      \ '-r', 'n'
  \ ],
  \ 'errorformat':
      \ '%A%f:%l:%c:%t: %m,' .
      \ '%A%f:%l: %m,' .
      \ '%A%f:(%l): %m,' .
      \ '%-Z%p^%.%#,' .
      \ '%-G%.%#',
  \ }
let g:neomake_python_enabled_makers = ['venvpylint']

if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

" remove the background so we use the shell color
hi Normal ctermbg=none
