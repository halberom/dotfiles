BAT_VERSION ?= v0.11.0
LIGHT_VERSION ?= 1.2

.PHONY: packages
packages:
	apt install \
		curl \
		docker \
		exa \
		git \
		htop \
		i3 \
		imagemagick \
		ipcalc \
		keychain \
		neovim \
		python3-pip \
		qutebrowser \
		rxvt-unicode \
		screen \
		scrot \
		ssh-askpass \
		stow \
		tcpdump \
		traceroute \
		virtualbox \
		zsh
	wget https://github.com/sharkdp/bat/releases/tag/${BAT_VERSION}/bat_${BAT_VERSION}_amd64.deb -O ~/Downloads/
	dpkg -i ~/Downloads/bat_${BAT_VERSION}_amd64.deb
	snap install pick-colour-picker

# backlight utility, much better than xbacklight, allows to set minimum
.PHONY: light
light:
	wget https://github.com/haikarainen/light/releases/download/v${LIGHT_VERSION}/light_${LIGHT_VERSION}_amd64.deb -O ~/Downloads/
	dpkg -i ~/Downloads/light_${LIGHT_VERSION}_amd64.deb
	cp ./90-backlight.rules /etc/udev/rules.d/

.PHONY: pip
pip:
	pip3 install --upgrade pip
	pip3 install raiseorlaunch

.PHONY: scripts
scripts:
	mkdir -p ~/bin
	cd ~/bin && wget https://raw.githubusercontent.com/rupa/z/master/z.sh

.PHONY: dotfiles
dotfiles:
	stow --no-folding -t $(HOME)/ homedir

.PHONY: liquidprompt
liquidprompt:
	git clone https://github.com/nojhan/liquidprompt.git ${HOME}/.liquidprompt

.PHONY: shell
shell:
	chsh -s /bin/zsh

.PHONY: gitpaths
git:
	mkdir -p ~/git/gitlab.com
	mkdir -p ~/git/github.com/polybar
	cd ~/git/github.com/polybar && git clone https://github.com/polybar/polybar.git

