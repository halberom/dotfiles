# Set the history file location
HISTFILE=~/.zsh_history
# specify number of entries in history for session
HISTSIZE=100000
# specify number of entries in history file
SAVEHIST=100000

# Appends every command to the history file once it is executed
setopt BANG_HIST                 # Treat the '!' character specially during expansion.
setopt EXTENDED_HISTORY          # Write the history file in the ":start:elapsed;command" format.
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY             # Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS          # Do not record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS      # Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS         # Do not display a line previously found.
setopt HIST_IGNORE_SPACE         # Do not record an entry starting with a space.
setopt HIST_SAVE_NO_DUPS         # Do not write duplicate entries in the history file.
setopt HIST_REDUCE_BLANKS        # Remove superfluous blanks before recording entry.
setopt HIST_VERIFY               # Do not execute immediately upon history expansion.

setopt COMPLETE_ALIASES

# load keychain
if [[ -d ~/.keychain ]]; then
  keychain -q -Q id_rsa
  [[ -f ~/.keychain/$(hostname)-sh  ]] && source ~/.keychain/$(hostname)-sh
else
  eval $(keychain --eval --agents ssh id_rsa)
fi

# get bash style directory tab completion
zstyle ':completion:*' rehash true
# make word selection like in bash, for tab'ing in folder paths
autoload -U select-word-style
select-word-style bash

# load zplug
source /usr/share/zplug/init.zsh

zplug "supercrabtree/k"
zplug "rupa/z", use:z.sh
zplug "nojhan/liquidprompt"

# local plugins
zplug "~/.zsh", from:local

# Set the priority when loading
# e.g., zsh-syntax-highlighting must be loaded
# after executing compinit command and sourcing other plugins
zplug "zsh-users/zsh-syntax-highlighting", defer:2

# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# Then, source plugins and add commands to $PATH
zplug load 

# source fzf
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# set up paths
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin

typeset -U PATH
export PATH="$HOME/.local/bin:$HOME/bin:$GOBIN:$PATH"

# * ~/.extra is used for settings that shouldn't be committed
for file in ~/.{aliases,dockerfunc,extra}; do
    [[ -r "$file" ]] && [[ -f "$file" ]] && source "$file"
done
unset file
